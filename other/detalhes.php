<!DOCTYPE html>
<html>
    <head>
        <title>Usando o UIKit</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/css/uikit.min.css" />
        <script src="../assets/js/uikit.min.js"></script>
        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/uikit-icons.min.js"></script>
      </head>
    <body>

         <h1 class="uk-margin-left">iPhone 11</h1>
 
        <div uk-grid>
        <!--Imagens do produto-->
            <div class="uk-width-1-2@m">
               <div class="uk-card uk-card-default uk-card-body">    
                
                        
                        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: scale;
                            autoplay: true; min-height: 100; max-height: 500" >

                            <ul class="uk-slideshow-items">
                                <li>
                                    <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
                                         <img src="../images/products/samsung.png" alt="" uk-cover>
                                    </div>
                                </li>
                                <li>
                                     <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-top-right">
                                         <img src="../images/products/mii.jpg" alt="" uk-cover>
                                     </div>
                                </li>
                                <li>
                                     <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-bottom-left">
                                         <img src="../images/products/iphone.jpg"alt="" uk-cover>
                                     </div>
                               </li>
                          </ul>

                         <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                         <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>


                    </div> <!--slideshow-->
                    
                    <!--outras imagens-->
    
                    <div class="uk-margin uk-width-expand@s">
                        <div class="uk-child-width-1-3@s" uk-grid>
                           
                           <div class="uk-grid-small uk-card uk-card-default uk-card-hover uk-card-body">
                               <a href="">
                                    <img src="../images/products/mii.jpg" alt="">
                                </a>
                           </div>

                            <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                               <a href="">
                                    <img src="../images/products/samsung.png" alt="">
                                </a>
                           </div>  
                        </div>

                    </div>
                </div> <!--card-->
            </div> <!--width-->

            <!--Especificações do dispositivo-->
             <div class="uk-width-expand@m">
                <div class="uk-card uk-card-secondary uk-card-body">
                    
                    <div class="uk-margin uk-card-title">Marca: Apple</div>
                
                    <div class="uk-margin-large-bottom uk-card-title">Preço: 70,000.00 MT</div>

                    <select class="uk-select">
                        <option value="">Quantidade</option>
                        <option>1</option>
                        <option>2</option>
                    </select>

                    <hr class="uk-divider-icon">

                    <div class="uk-margin uk-card-title">Descrição</div>
                        <p class=""> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


                <div class="uk-column-1-2@s uk-visible@s">
                    <button class="uk-button uk-button-secondary">Adicionar ao carrinho <span uk-icon="cart"></span></button> 
                    <button class="uk-button uk-button-primary">Comprar</button> 
  
                </div>

                  <div class="uk-column-1-2@s uk-hidden@s">
                    <button class="uk-button uk-button-secondary">Adicionar ao carrinho <span uk-icon="cart"></span></button> 
                    <button class="uk-margin uk-button uk-button-primary">Comprar</button> 
  
                </div> 
 
               
                 </div>
            </div>
        </div>
    </body>
</html>


