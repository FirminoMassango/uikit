<!DOCTYPE html>
<html>
    <head>
        <title>Usando o UIKit</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/css/uikit.min.css" />
        <script src="../assets/js/uikit.min.js"></script>
        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/uikit-icons.min.js"></script>
      </head>
    <body>
        <div class="uk-card uk-card-default uk-width-1-3@m uk-position-center">
            <div class="uk-card-header uk-text-center uk-background-secondary">
                <img src="../images/logo/logo.svg" alt="">
            </div>

            <div class="uk-card-body uk-text-center">
            

            <!--Card Body start-->
                <ul class="uk-flex-center" uk-switcher="animation: uk-animation-slide-left-medium, uk-animation-slide-right-medium" uk-tab>
                    <li><a href="#">Iniciar Sessão</a></li>
                    <li><a href="#">Criar Conta</a></li>
                </ul>

                <ul class="uk-switcher uk-margin">
                    <li><?php include 'login.php'?></li>
                    <li><?php include 'criar_conta.php'?></li>
                </ul>
            </div>
            <!--Card Body end -->
            <div class="uk-card-footer uk-text-center uk-background-secondary">
               <button class="uk-button uk-button-text uk-light" type="submit">Confirmar </button>
               <!-- &copy 2020-->
    
            </div>
        </div>
    </body>
</html>


