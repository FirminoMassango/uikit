<!DOCTYPE html>
<html>
    <head>
        <title>Usando o UIKit</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="images/favicon/favicon.png"/>
        <link rel="stylesheet" href="assets/css/uikit.min.css" />
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/uikit.min.js"></script>
        <script src="assets/js/uikit-icons.min.js"></script>
    </head>
    <body>
            <!--off-canvas-->
             <div id="sidenav" class="uk-offcanvas" uk-offcanvas>
                <div class="uk-offcanvas-bar">
                    <ul class="uk-nav">
                        <li class="uk-active"><a href="#">Produtos</a></li>
                        <li><a href="#">Suporte</a></li>
                        <li><a href="#">Contacte-nos</a></li>
                        <li class="uk-margin-small-top" ><span uk-icon="cart">0 </span></li>
                        <li class="uk-margin">
                            <div>
                                <form class="uk-search uk-search-default">
                                    <a href="" class="uk-search-icon-flip" uk-search-icon></a>
                                    <input class="uk-search-input" type="search" placeholder="Pesquisar...">
                                </form>
                            </div>
                        </li>
                        <li>
                            <hr>
                        </li>
                        <li>
                            <div>
                                <a href="#" class="uk-link-reset uk-margin-small-right" uk-tooltip="Clique para Iniciar Sessão">Iniciar Sessão</a>
                                <span>|</span>
                                <a href="#" class="uk-link-reset uk-margin-small-left" uk-tooltip="Clique para Criar Conta">Criar Conta</a>
                            </div>
                        </li> 
                    </ul>
                </div>
            </div>

            <a class="uk-navbar-toggle uk-hidden@m uk-inline uk-margin" uk-toggle="target: #sidenav"  uk-navbar-toggle-icon></a>
            <!--navbar-->
             <nav class="uk-navbar-container uk-visible@m" uk-navbar>
               
                 <!--lado esquerdo-->
                <div class="nav-overlay uk-navbar-left">
                    
                    <a class="uk-navbar-item uk-logo" href="#"><img src="images/logo/logo.svg" alt=""></a>

                    <ul class="uk-navbar-nav">
                        <li class="uk-active"><a href="#">Produtos</a></li>
                        <li><a href="#">Suporte</a></li>
                        <li><a href="#">Contacte-nos</a></li>
                    </ul>

                </div>
            <!--nav lado direito-->
                <div class="nav-overlay uk-navbar-right">
                    <!--carrinha-->
                        <div class="uk-navbar-item">
                            <span uk-icon="cart">0 </span>
                        </div>
                
                    <!--Conta-->
                    <div class="uk-navbar-item">
                        <span uk-icon="user" style="width:16px;"></span>
                        <a href="account/account_switch.php" class="uk-text-muted uk-text-uppercase uk-margin-small-left" uk-tooltip="Clique para criar ou entrar na conta">Iniciar Sessão</a>
                    </div>
                    
                    <!--pesquisa-->
                    <a class="uk-navbar-toggle" uk-search-icon uk-toggle="target: .nav-overlay; animation: uk-animation-fade" href="#" uk-tooltip="Clique para pesquisar"></a>

                </div>
                    

                <div class="nav-overlay uk-navbar-left uk-flex-1" hidden>
                    
                    <div class="uk-navbar-item uk-width-expand">
                        <form class="uk-search uk-search-navbar uk-width-1-1">
                            <input class="uk-search-input" type="search" placeholder="Pesquisar..." autofocus>
                        </form>
                    </div>

                        <a class="uk-navbar-toggle" uk-close uk-toggle="target: .nav-overlay; animation: uk-animation-fade" href="#"></a>

                    </div>

                </nav>  


            <!--Slideshow-->
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: scale;
                        autoplay: true; min-height: 100; max-height: 500" >

                <ul class="uk-slideshow-items">
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
                             <img src="images/banner/banner1.png" alt="" uk-cover>
                        </div>
                    </li>
                    <li>
                         <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-top-right">
                             <img src="images/banner/banner2.jpg" alt="" uk-cover>
                         </div>
                    </li>
                    <li>
                         <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-bottom-left">
                             <img src="images/banner/banner3.jpg"alt="" uk-cover>
                         </div>
                   </li>
              </ul>

             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

        </div>

            

            <!--Título-->
            <h1 class="uk-text-center uk-margin-large-top uk-margin-xlarge-bottom">As melhores novidades estão aqui.</h1>
            <!--<h2 class="uk-text-center">Melhores preços do mercado.</h2>--> 
            <!--Modal-->

            <div id="modal-example" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

                <button class="uk-modal-close-default" type="button" uk-close></button>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            </div>
	</div>
	<!--Importando o arquivo que contém o central da página-->
	<?php include 'includes/produtos.php'?>

 <!--Importando o rodapé--> 
	<?php include "includes/footer.php"?>

