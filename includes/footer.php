
       <div class="uk-padding-small uk-column-1-3@s uk-background-muted uk-text-center">
          <div>
               <ul class="uk-list uk-link-text">
                  <li>
                      <h4>Conteúdo1</h4>
                  </li>  
                  <li><a href="#">Col1</a></li>
                  <li><a href="#">Col1</a></li>
                  <li><a href="#">Col1</a></li>
              </ul> 

          </div>
          <div >
               <ul class="uk-list uk-link-text">
                  <li>
                      <h4>Conteúdo2</h4>
                  </li>  
                  <li><a href="#">Col2</a></li>
                  <li><a href="#">Col2</a></li>
                  <li><a href="#">Col2</a></li>
              </ul> 

          </div>
          <div>  
              <ul class="uk-list uk-link-text">
                  <li>
                      <h4>Siga-nos</h4>
                  </li>  
                  <li><a href="#" uk-icon="facebook"></a></li>
                  <li><a href="#" uk-icon="twitter"></a></li>
                  <li><a href="#" uk-icon="instagram"></a></li>
              </ul> 
          </div>

      </div>
      <div class="uk-text-center uk-light uk-background-secondary"> &copy; 2020 - TheClik. Todos os Direitos Reservados</div>
   
   </body> 
</html>
