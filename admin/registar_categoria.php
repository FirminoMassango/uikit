<!DOCTYPE html>
<html>
    <head>
        <title>Usando o UIKit</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.min.css" />
        <script src="js/uikit.min.js"></script>
      </head>
    <body>
        <form class="uk-margin-right uk-margin-left">
            <fieldset class="uk-fieldset">

                <legend class="uk-legend">Categoria</legend>

                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Nome da categoria">
                </div>
                
                <div>
                    <button class="uk-button uk-button-primary" type="submit">Confirmar</button>
                </div>

            </fieldset>
        </form> 
    </body>
</html>


