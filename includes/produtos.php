<!--Categoria e Produtos--> 
<div class="uk-margin-large" uk-grid>

    <!--Lado Esquerdo-->
    <div class="uk-text-left uk-margin-left uk-width-1-4@s uk-background-muted">
        <ul class="uk-nav-primary uk-nav-parent-icon" uk-nav>
            <li class="uk-parent">
                <a href="#">Categoria</a>
                <ul class="uk-nav-sub">
                    <li><a href="#">SmartPhone</a></li>
                    <li><a href="#">Tablet</a></li>
                </ul>
            </li>
            <li class="uk-parent">
                <a href="#">Marca</a>
                <ul class="uk-nav-sub">
                    <li><a href="#">Samsung span.uk-badge </a></li>
                    <li><a href="#">Apple</a></li>
                     <li><a href="#">Xiaomi</a></li>
                     <li><a href="#">LG</a></li>
                     <li><a href="#">Motorola</a></li>
                     <li><a href="#">Vodafone</a></li>
                     <li><a href="#">HTC</a></li>
                     <li><a href="#">Lenovo</a></li>
                     <li><a href="#">Azus</a></li>
                     <li><a href="#">HP</a></li>
                     <li><a href="#">Tecno</a></li>
                     <li><a href="#">Infinix</a></li>


                </ul>
                </li>
                 <li class="uk-parent">
                    <a href="#">Preço</a>
                    <ul class="uk-nav-sub">
                        <li>
                            <form action="">
                                <input class="uk-input uk-form-width-small uk-form-small" type="number" name=""
                                id="" placeholder="valor inicial" min=1>
                                <input class="uk-margin uk-input uk-form-width-small uk-form-small" type="number" name=""
                                id="" placeholder="limite" min=1>
                                <input class="uk-margin uk-button uk-button-primary uk-button-small" type="submit" value="Confirmar">
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div> 

        <!--Lado Direito-->
    <div class="uk-width-expand@s">
       <h5 class="uk-text-bold uk-text-uppercase uk-text-success">Mais Vendidos</h5>       
       <!--card Responsivo-->
        <div class="uk-margin uk-grid-small uk-child-width-1-3@s uk-child-width-1-4@m uk-flex-center
            uk-text-center uk-margin-right" uk-grid
            uk-scrollspy="cls:uk-animation-fade;target: .uk-card; delay: 200; repeat: true">    
            
            <div>
             <a href="#modal-example" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">Xiaomi Mi9</h3>
                    <img src="images/products/mii.jpg" alt="mi 9">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                    <!-- <button class="uk-button uk-button-default">DETALHES</button>-->
                </div>
             </a>
            </div>
            <div>
                <a href="#modal-example" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">iPhone 11</h3>
                    <img src="images/products/iphone.jpg" alt="mi 9">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                </div>
                </a>
            </div>
           <div>
             <a href="#modal-example" class="uk-link-reset" uk-toggle>

                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">Galaxy S10</h3>
                    <img src="images/products/samsung.png" alt="galaxy s10">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                </div>
             </a>
            </div>
            
            <div>
             <a href="#modal-example" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-body">
                   
                    <img src="images/products/mii.jpg" alt="mi 9">
                    <h5 class="uk-text-bold">Xiaomi Mi 9</h5>
                     <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                    <!-- <button class="uk-button uk-button-default">DETALHES</button>-->
                    
                </div>
             </a>
            </div>
            <div>
                <a href="other/detalhes.php" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">iPhone 11</h3>
                    <img src="images/products/iphone.jpg" alt="mi 9">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                </div>
                </a>
            </div>
           <div>
             <a href="other/detalhes.php" class="uk-link-reset" uk-toggle>

                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">Galaxy S10</h3>
                    <img src="images/products/samsung.png" alt="galaxy s10">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                </div>
             </a>
            </div> 
             <div>
                <a href="other/detalhes.php" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">iPhone 11</h3>
                    <img src="images/products/iphone.jpg" alt="mi 9">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                </div>
                </a>
            </div>
           <div>
             <a href="other/detalhes.php" class="uk-link-reset" uk-toggle>

                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-card-title">Galaxy S10</h3>
                    <img src="images/products/samsung.png" alt="galaxy s10">
                    <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                </div>
             </a>
            </div>              
        </div>


        <h5 class="uk-text-bold uk-margin-xlarge-top uk-text-uppercase uk-text-success">Outros</h5>       


         <!--card Responsivo-->
        <div class="uk-margin uk-grid-small uk-child-width-1-3@s uk-child-width-1-4@m uk-flex-center
            uk-text-center uk-margin-right" uk-grid>

          <!--  uk-scrollspy="cls:uk-animation-fade;target: .uk-card; delay: 200; repeat: true"-->    
            
             <div>
             <a href="other/detalhes.php" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                   
                    <img src="images/products/iphone.jpg" alt="mi 9">
                    <h5 class="uk-text-bold">iPhone 11</h5>
                     <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                    <!-- <button class="uk-button uk-button-default">DETALHES</button>-->
                    
                </div>
             </a>
            </div>
              <div>
             <a href="other/detalhes.php" class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                   
                    <img src="images/products/mii.jpg" alt="mi 9">
                    <h5 class="uk-text-bold">Xiaomi Mi 9</h5>
                     <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                    <!-- <button class="uk-button uk-button-default">DETALHES</button>-->
                    
                </div>
             </a>
            </div>
              <div>
             <a href="other/detalhes.php"class="uk-link-reset">
                <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                   
                    <img src="images/products/samsung.png" alt="mi 9">
                    <h5 class="uk-text-bold">Galaxy S10</h5>
                     <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                    <!-- <button class="uk-button uk-button-default">DETALHES</button>-->
                    
                </div>
             </a>
            </div>
              <div>
             <a href="other/detalhes.php"class="uk-link-reset" uk-toggle>
                <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                  <div class="uk-media_top">
                       <img src="images/products/mii.jpg" alt="mi 9">

                  </div> 
                    <h5 class="uk-text-bold">Xiaomi Mi 9</h5>
                     <h5 class="uk-text-danger uk-text-bold">50.000,00 MZN</h5>
                    <!-- <button class="uk-button uk-button-default">DETALHES</button>-->
                    
                </div>
             </a>
            </div>
             

                         
        </div>





    </div>
</div>	
<!--Paginação-->

<ul class="uk-pagination uk-margin-large uk-flex-center" uk-margin>
    <li><a href="#"><span uk-pagination-previous></span></a></li>
    <li><a href="#">1</a></li>
    <li class="uk-disabled"><span>...</span></li>
    <li><a href="#">5</a></li>
    <li><a href="#">6</a></li>
    <li class="uk-active"><span>7</span></li>
    <li><a href="#">8</a></li>
    <li><a href="#"><span uk-pagination-next></span></a></li>
</ul></div>

<!--Ir para o topo-->
<div class="uk-position-bottom-right  uk-position-z-index uk-position-fixed uk-background-secondary uk-border-rounded uk-margin-right" uk-tooltip="Ir ao topo">
<a href="" uk-totop uk-scroll></a>	
</div>

<hr class="uk-divider-icon">

