        <form action="">
            <div class="uk-margin">
                <div class="uk-inline">
                    <span class="uk-form-icon" uk-icon="icon: user"></span>
                    <input class="uk-input" type="text" placeholder="nome de utilizador" autofocus>
                </div>
            </div>

            <div class="uk-margin">
                <div class="uk-inline">
                    <span class="uk-form-icon" uk-icon="icon: lock"></span>
                    <input class="uk-input" type="password" placeholder="senha">
                </div>
            </div>
            
            <div class="uk-margin">
                <div class="uk-inline">
                    <span class="uk-form-icon" uk-icon="icon: lock"></span>
                    <input class="uk-input" type="password" placeholder="confirme a senha">
                </div>
            </div>
            <a href="../index.php" class="uk-button uk-button-text">Esqueceu-se da senha?</a>
<!--            <div class="uk-margin">
                    <button class="uk-button uk-button-primary uk-width-expand" type="submit">Confirmar</button>
            </div>-->
        </form>


