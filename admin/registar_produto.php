<!DOCTYPE html>
<html>
    <head>
        <title>Usando o UIKit</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.min.css" />
        <script src="js/uikit.min.js"></script>
      </head>
    <body>
        <form class="uk-margin-right uk-margin-left">
            <fieldset class="uk-fieldset">

                <legend class="uk-legend">Produto</legend>

                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Nome do produto">
                </div>
                
                <div class="" uk-grid>
                     <div class="uk-width-1-3@m">
                        <select class="uk-select">
                            <option value="">Marca</option>
                            <option>Option 01</option>
                            <option>Option 02</option>
                        </select>
                     </div>
                     <div class="uk-width-1-3@m">
                        <select class="uk-select">
                            <option value="">Categoria</option>
                            <option>Option 01</option>
                            <option>Option 02</option>
                        </select>
                     </div>
                     <div class="uk-width-1-3@m">
                        <input class="uk-input" type="text" name="" id="" placeholder="Quantidade">   
                     </div>



 
                </div>
                
                <div uk-grid>
                    <div>
                     <div uk-form-custom="target: true">
                        <input type="file">
                        <input class="uk-input uk-form-width-large" type="text" placeholder="Select file" disabled>
                    </div>
                </div>
                <div class="uk-width-1-3@s">
                    <input class="uk-input" type="text" name="" id="" placeholder="Preço">
                </div>

                </div>
                               
                
                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="5" placeholder="Descrição"></textarea>
                </div>
                <div>
                    <button class="uk-button uk-button-primary" type="submit">Confirmar</button>
                </div>

            </fieldset>
        </form> 
    </body>
</html>


