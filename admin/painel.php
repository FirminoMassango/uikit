<!DOCTYPE html>
<html>
    <head>
        <title>Usando o UIKit</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="../images/favicon/favicon.png"/>
        <link rel="stylesheet" href="../assets/css/uikit.min.css" />
        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/uikit.min.js"></script>
        <script src="../assets/js/uikit-icons.min.js"></script>
      </head>
    <body>

        <!--Off-canvas-->
        
        <a uk-toggle="target: #offcanvas-usage" class="uk-margin-top uk-navbar-toggle uk-inline uk-hidden@m" uk-navbar-toggle-icon></a>

        <div id="offcanvas-usage" uk-offcanvas>
            <div class="uk-offcanvas-bar">

                <button class="uk-offcanvas-close" type="button" uk-close></button>
                <ul class="uk-nav uk-margin-xlarge-top">
                   <li><a href="#">Notificações</a></li>
                   <li><a href="../index.php">Visitar o Site</a></li>
                   <li><a href="#" uk-icon="sign-out">Encerrar Sessão</a></li>
                </ul>
           </div>
        </div>


        <!--Navbar-->
        <nav class="uk-navbar-container uk-visible@m" uk-navbar>

            <div class="uk-navbar-left">
                 <a class="uk-navbar-item uk-logo" href="#">
                    <img src="../images/logo/logo.svg" alt="">
                </a>
                <ul class="uk-navbar-nav">
                    <li><a href="#">Notificações</a></li>
                    <li><a href="../index.php">Visitar o Site</a></li>
                </ul>

            </div>

            <div class="uk-navbar-right">
                <ul class="uk-navbar-nav">
                    <li><a href="#" uk-icon="sign-out">Encerrar Sessão</a></li>
                </ul>

            </div>

        </nav>  

        <!--Switcher para resoluções pequenas-->       
         <div class="uk-margin uk-hidden@m">
            <div uk-grid>
                <div class="uk-background-muted uk-width-auto@m">
                    <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                        <li><a href="#">Adicionar Marca</a></li>
                        <li><a href="#">Adicionar Categoria</a></li>
                        <li><a href="#">Adicionar Produto</a></li>
                    </ul>
                </div>
         
                <div class="uk-width-expand@m">
                    <ul id="component-tab-left" class="uk-switcher">
                        <li><?php include 'registar_marca.php'?></li>
                        <li><?php include 'registar_categoria.php'?></li>
                        <li><?php include 'registar_produto.php'?></li>
                        </ul>
                </div>
            </div>
         </div>
        
          <!--Switcher para resoluções maiores-->       
         <div class="uk-margin uk-visible@m">
            <div uk-grid>
                <div class="uk-background-muted uk-width-auto@m uk-height-viewport">
                    <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                        <li><a href="#">Adicionar Marca</a></li>
                        <li><a href="#">Adicionar Categoria</a></li>
                        <li><a href="#">Adicionar Produto</a></li>
                    </ul>
                </div>
         
                <div class="uk-width-expand@m">
                    <ul id="component-tab-left" class="uk-switcher">
                        <li><?php include 'registar_marca.php'?></li>
                        <li><?php include 'registar_categoria.php'?></li>
                        <li><?php include 'registar_produto.php'?></li>
                        </ul>
                </div>
            </div>
         </div>


    </body>
</html>


